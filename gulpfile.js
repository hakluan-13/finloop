const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const rename = require('gulp-rename');
const cleanCSS = require('gulp-clean-css');
const purgeCSS = require('gulp-purgecss');
const clean = require('gulp-clean');
const autoprefixer = require('gulp-autoprefixer');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const htmlmin = require('gulp-htmlmin');
const babel = require('gulp-babel');
const fs = require('fs');
const imagemin = require('gulp-imagemin');
const merge = require('merge-stream');

const browserSync = require('browser-sync').create();

const paths = {

	public: {
		www : 'public'
	},
	src: {
		root	: 'public',
		html	: 'public/*.html',
		css		: 'public/css/',
		js		: 'public/js/',
		vendors : 'public/vendors/**/*.*',
		imgs 	: 'public/imgs/**/*.+(png|jpg|gif|svg)',
		fonts   : 'public/fonts/*.*',
		scss	: [
					'public/scss/main.scss',
					'public/scss/**/**.scss',
				  ]

	},
	union:{
		js 		: [
					'public/js-dev/main.js',
					'public/js-dev/counter.js'
				  ],
		css 	: 'public/css/',
		vendors : 'public/vendors/'
	},
	dist: {
		root	: 'public/dist',
		css		: 'public/dist/css',
		js		: 'public/dist/js',
		imgs	: 'public/dist/imgs',
		vendors : 'public/dist/vendors',
		fonts   : 'public/dist/fonts',
	},
	vendors: ['jquery/dist', 'bootstrap/dist', 'animate.css/', 'jquery.counterup/', 'slick-carousel/']

};


// compresión de images (JPEG, PNG, GIF, SVG, JPG)

gulp.task('images', () => {
	return gulp
			.src(paths.src.imgs)
			.pipe(
				imagemin([
					imagemin.gifsicle({
						interlaced: true,
					}),
					imagemin.mozjpeg({
						quality: 75,
						progressive: true,
					}),
					imagemin.optipng({
						optimizationLevel: 5,
					}),
					imagemin.svgo({
						plugins: [
							{
								removeViewBox: true,
							},
							{
								cleanupIDs: false,
							},
						],
					}),
				]),
			)
			.pipe(gulp.dest(paths.dist.imgs));
});


// Compilar archivos sass
gulp.task('sass', () => {

	return gulp.src(paths.src.scss)
		.pipe(
			sass({
				outputStyle: 'expanded',
			}).on('error',  sass.logError),
		)
		.pipe(autoprefixer())
		//.pipe(concat('app.css')) /* concatenamos todos los archivos */
		.pipe(
			rename({
				suffix: '.min',
			}),
		)
		.pipe( gulp.dest( paths.src.css )) 
		.pipe(browserSync.stream());

});


gulp.task('js-dev', () => {

	return gulp
			.src( paths.union.js )
			.pipe(
				babel({
					presets: ['@babel/preset-env'],
				}),
			)
			.pipe(concat('app.js'))
			.pipe(
				rename({
					suffix: '.min',
				}),
			)
			.pipe(gulp.dest(paths.src.js))
			.pipe(browserSync.stream());

});



gulp.task('vendors', function() {

  if (fs.existsSync( paths.src.vendors )) {
  	fs.rm(paths.union.vendors, { recursive: true }, () => console.log('vendors delete'));
  }

  return merge(paths.vendors.map(function(vendor) {
    return gulp.src('node_modules/' + vendor + '/**/*.min.*')
      .pipe(gulp.dest(paths.union.vendors + vendor.replace(/\/.*/, '')));
  }));
});

// Copiar los vendors al dist
gulp.task('copy-vendors', () => {
	return gulp.src(paths.src.vendors).pipe(gulp.dest(paths.dist.vendors));
});


// Minificar y cobinar archivos css
gulp.task('css', () => {

	return gulp
		.src(paths.src.css + '*.min.css')
		//.src(paths.src.css + 'app.min.css')
		//.pipe(concat('app.css'))
		.pipe(
			cleanCSS({
				compatibility: 'ie8',
			}),
		)
		//.pipe(concat('app.css'))
		.pipe(purgeCSS({
			content: ['public/**/*.html']
		}))
		/*.pipe(
			rename({
				suffix: '.min',
			}),
		)*/
		.pipe(gulp.dest(paths.dist.css))
});


// Minificar y combinar archivos js

gulp.task('js', () => {

	return gulp
			//.src( paths.src.js + '*.js' )
			.src( paths.src.js + '*.min.js' )
			.pipe(
				babel({
					presets: ['@babel/preset-env'],
				}),
			)
			//.pipe(concat('app.js'))
			.pipe(uglify())
			/*.pipe(
				rename({
					suffix: '.min',
				}),
			)*/
			.pipe(gulp.dest(paths.dist.js))
			.pipe(browserSync.stream());

});


// Minificar los archivos html
gulp.task('html', () => {
	return gulp.src( paths.src.html )
    	.pipe(htmlmin({ collapseWhitespace: true }))
    	.pipe(gulp.dest(paths.dist.root));

});

gulp.task('fonts', () => {
	return gulp.src( paths.src.fonts )
    	.pipe(gulp.dest( paths.dist.fonts ));

});

// limpiar el dist
gulp.task('clean', function () {

	if (!fs.existsSync( paths.dist.root )) {
		fs.mkdirSync(paths.dist.root, {
			recursive: true
		});
	}

	return gulp.src(paths.dist.root).pipe(clean());
});

// Preparar código para producción
gulp.task('build', gulp.series('clean', 'sass', 'js-dev', 'css', 'js', 'html', 'images', 'copy-vendors', 'fonts' ));


gulp.task('serve',() => {
	
	browserSync.init({
		server: {
			baseDir: paths.public.www
		}
	});


	// Reloading ...
	gulp.watch(paths.src.scss, gulp.series('sass'));
	gulp.watch(paths.union.js, gulp.series('js-dev'));
	gulp.watch(paths.src.html).on('change', browserSync.reload);
});	


gulp.task('default', gulp.series('sass', 'js-dev', 'vendors', 'serve' ));