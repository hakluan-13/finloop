$(function(){

	/* Gobal */
	var $window = $(window);
	var $nav = $(".navbar");
	
	/* Elements for animated */
	var $imageparallaxUp = $('.image_parallax_up');
	var $imageparallaxDown = $('.image_parallax_down');
	var $imageparallaxSlide = $('.image_parallax_up_slide');
	var $inview = $('.inview');	
	var isMobile = false;	
	var imagesscrollinghorizontal = ['originacion.jpg','administracion.jpg','mitigacion-de-riesgos.png'];

	/* Scrolles*/
	var $scrollverticalContent = $('#scroll-vertical-content');
	var $horizontal = $('.horizontal');

	/*Carrusles*/
	var $carruselAwards = $('.carrusel');
	var $carruselTestimonials = $('#testimonials-slider');
	var imagebarslider = $('.image-bar #images');
	var $sliderTeam = $('#slider');


	$scrollverticalContent.slick({
		arrows: !1,
		cssEase: "linear",
		dots: !0,
		fade: !0,
		infinite: !0,
		speed: 500
	}).on('beforeChange', function(event, slick, currentSlide, nextSlide){
		if( nextSlide == 2){
			$('#images').css('display', 'none');
			$('.download').css('display', 'flex');
		}
		else{
			$('#images').css('display', 'flex');
			$('.download').css('display', 'none');
			imagebarslider.attr('src', '/imgs/page1/'+ imagesscrollinghorizontal[nextSlide] );
		}
	});

	$carruselTestimonials.slick({
		dots: true,
		arrows: false,
		autoplaySpeed: 2500,
		slidesToScroll: 1,
        slidesToShow: 1,
		infinite: true,
		autoplay: true,
	});


	$sliderTeam.slick({
		dots: false,
		arrows: true,
		autoplaySpeed: 2500,
		slidesToScroll: 1,
        slidesToShow: 1,
		infinite: true,
		autoplay: false,
		fade: true,
		prevArrow: $('.prev'),
		nextArrow: $('.next')
	});


	/* functions */
	function check_windows_load() {
		var window_height = $window.height();
		var window_top_position = $window.scrollTop();
		var window_bottom_position = (window_top_position + window_height);

		if($( '#section-scroll-vertical' ).get(0) != undefined){
			scrollingActiveVertical(window_height, window_top_position, window_bottom_position);
		}

		if( $('.horizontal').get(0) != undefined ){
			scrollingActiveHorizontal();
		}
				
		/* inview */
		$.each($inview, function() {
			var $element = $(this);
			var element_height = $element.outerHeight();
			var element_top_position = $element.offset().top;
			var element_bottom_position = (element_top_position + element_height);

			if ((element_bottom_position >= window_top_position) && (element_top_position <= window_bottom_position)) {
				$element.addClass('animate__animated animate__fadeInUp animate__delay-1s');
			}
		});

		/* parallax up*/
		$.each( $imageparallaxUp, function(){
			var $element = $(this);
			var element_height = $element.outerHeight();
			var element_top_position = $element.offset().top;
			var element_bottom_position = (element_top_position + element_height);

			if ((element_bottom_position >= window_top_position  ) && (element_top_position <= window_bottom_position  )) {
				$element.css('bottom', ( window_bottom_position - element_bottom_position ) * 0.15 + 'px');
			}
		});	

		/* parallax up*/
		$.each( $imageparallaxDown, function(){
			var $element = $(this);
			var element_height = $element.outerHeight();
			var element_top_position = $element.offset().top;
			var element_bottom_position = (element_top_position + element_height);

			if ((element_bottom_position >= window_top_position  ) && (element_top_position <= window_bottom_position  )) {
				$element.css('bottom', '-'+ ( window_bottom_position - element_bottom_position ) * 0.6 + 'px');
			}
		});	

		/* parallax slide*/
		$.each( $imageparallaxSlide, function(){
			var $element = $(this);
			var element_height = $element.outerHeight();
			var element_top_position = $element.offset().top;
			var element_bottom_position = (element_top_position + element_height);

			if ((element_bottom_position >= window_top_position  ) && (element_top_position <= window_bottom_position  ) && !isMobile) {
				$element.css('right', '' + ( window_bottom_position - element_bottom_position ) * 0.1 + 'px');
			}
		});
	}

	function scrollingActiveHorizontal(){
		var scrollTop = $(window).scrollTop();
  
		if (scrollTop >= $('.horizontal').offset().top && 
			scrollTop <= $('.horizontal').offset().top + $('.horizontal').outerHeight(true) - $(window).height() ) {
		 
		  var startPoint = scrollTop - $('.horizontal').offset().top;
		  var endPoint = $(window).height() - $('.horizontal').offset().top + $('.horizontal').outerHeight(true) ;
		  
		  var translateX = ( startPoint / endPoint ) * 90
		  
		  $('.horizontal .horizontal-container').addClass('sticky');
		  $('.horizontal .horizontal-content').css({
			'transform': 'translateX(-'+translateX+'%)'
		  });
		  
		} else if (scrollTop >= $('.horizontal').offset().top + $('.horizontal').outerHeight(true) - $(window).height()) {
		  
			$('.horizontal .horizontal-container')
			  .removeClass('sticky')
			  .addClass('post-sticky');
		  
		} else {
		  
		  $('.horizontal .horizontal-container')
			.removeClass('post-sticky')
			.removeClass('sticky')
			.addClass('pre-sticky')
		}
	}

	function resizeHorizontalContainer() {
		$horizontal.each(function () {
		  var containerWidth = $(this).find('.horizontal-container').outerWidth(true);
		  var totalWidth = 0;
		  
		  $(this).find('.horizontal-item').each(function () {
			var width = parseInt($(this).outerWidth(true)); 
			totalWidth += width;
		  });
	  
		  $(this).css({ 'height': totalWidth });
		  $(this).find('.horizontal-content').css({ 'width': totalWidth - $(window).width() });
	  
		});  
	}

	function scrollingActiveVertical( window_height, window_top_position, window_bottom_position){
		var $scrollvertical = $('#scroll-vertical');
		var $scrollVerticalContent = $( '#section-scroll-vertical' );
		var content_bottom_position = $scrollVerticalContent.offset().top + $scrollVerticalContent.outerHeight();
		var dots = $('.slick-dots');
		var partners = $('#partners_scroll').get(0);
		
		if( window_top_position >= ($scrollvertical.offset().top  - 100)){
			window_bottom_position  <= content_bottom_position 
			? $('#tecnologia').css({'position': 'fixed', 'top': partners != undefined ? '0px' : '50px'})
			: $('#tecnologia').css({'position': 'relative', 'top': 'unset', 'bottom': '-'+$scrollVerticalContent.height() +'px'});

			var npositionwindows = window_top_position + 100;
			
			if( window_bottom_position <= $('.d-one').offset().top){
				dots.removeClass('first');
				dots.find('li').get(0).children[0].click();
				dots.find('li').get(1).children[0].classList.remove('slick-checking');
				$('.img1-left-scrolling').css({'transform':' translate(0px, 0px)', 'opacity': '1'});
				$('.img1-right-scrolling').css({'transform':' translate(0px, 0px)', 'opacity': '1'});
			}

			if( npositionwindows >= $('.d-one').offset().top && $('.d-one').offset().top + $('.d-one').height()  >= npositionwindows){ 
				dots.addClass('first');
				dots.removeClass('complete');
				dots.find('li').get(0).children[0].classList.add('slick-checking');
				dots.find('li').get(1).children[0].click();
				$('.img2-left-scrolling').css({'transform':'translate(0px, 0px)', 'opacity': '1'});
				$('.img2-right-scrolling').css({'transform':' translate(0px, 0px)', 'opacity': '1'});
			}

			if( npositionwindows >= $('.d-two').offset().top && $('.d-two').offset().top + $('.d-two').height()  >= npositionwindows){ 
				dots.addClass('complete');
				dots.find('li').get(1).children[0].classList.add('slick-checking');
				dots.find('li').get(2).children[0].click();
			}
		}
		else{
			$('.img1-left-scrolling').css({'transform':'translate(200%, 200%) rotate(-130deg)', 'opacity': '0'});
			$('.img1-right-scrolling').css({'transform':' translate(-100%, -300%)', 'opacity': '0'});
			$('.img2-left-scrolling').css({'transform':'translate(200%, 200%) rotate(-130deg)', 'opacity': '0'});
			$('.img2-right-scrolling').css({'transform':' translate(-100%, -300%)', 'opacity': '0'});
			$('#tecnologia').css({'position': 'absolute', 'top': '0px', 'bottom': 'unset' });
		}
	}
	
	$(document).scroll(function () {
		var window_top_position = $window.scrollTop();
		$nav.toggleClass('nav_scroll animated animate__fadeInDown', window_top_position > $nav.height());
		if( window_top_position > $nav.height()){
			$nav.addClass('animate__animated nav_scroll');
			$('.logo img').attr('src', '/imgs/logos/finloop.svg');
			$('#iniciar_session').removeClass('button-menu').addClass('button-orange');
		}
		else{
			$nav.removeClass('animate__animated nav_scroll');
			$('.logo img').attr('src', '/imgs/logos/finloop-w.svg');
			$('#iniciar_session').removeClass('button-orange').addClass('button-menu');
		}
	});


	var $help = $('#section-podemos-ayudarte');
	$('#click-podemos-ayudarte').click(function(){
		var $navigate = $(this).attr('href');
		if( $navigate === '#'){
			$(window).scrollTop( $help.offset().top );
		}
		else{
			window.location = 'index.html#podemos-ayudarte-redirect';
		}
	});

	/* Rezize */
	$(window).on('load resize', function() {
		var href = window.location.href.split('#');
		
		resizeHorizontalContainer();	
		
		if( href[1] == 'podemos-ayudarte-redirect' ){
			$(window).scrollTop( $help.offset().top );
		}

		if ($(window).width() > 900) { // 767
			if ($carruselAwards.hasClass('slick-initialized')) {
				$carruselAwards.slick('unslick');
			}
			return;
		  }
		  if (!$carruselAwards.hasClass('slick-initialized')) {
			isMobile = true;
			return $carruselAwards.slick({
				slidesToShow: 3,
				slidesToScroll: 1,
				autoplay: true,
				autoplaySpeed: 1000,
				mobileFirst: true,
				dots: false,
				arrows: false
			});
		  }
	});
	
	
	/*  Execute */
	$window.on('load scroll', check_windows_load);
	$window.trigger('scroll');
});